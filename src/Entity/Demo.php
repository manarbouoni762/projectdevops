<?php

namespace App\Entity;

use App\Repository\DemoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DemoRepository::class)
 */
class Demo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Demo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDemo(): ?string
    {
        return $this->Demo;
    }

    public function setDemo(string $Demo): self
    {
        $this->Demo = $Demo;

        return $this;
    }
}
