<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LocationTest extends WebTestCase
{
public function testShouldDisplayLocationIndex()
{


$client = static::createClient();
$client->followRedirects();
$crawler = $client->request('GET', '/location');

$this->assertResponseIsSuccessful();
$this->assertSelectorTextContains('h1', 'Location index');
}

public function testShouldDisplayCreateNewLocation()
{
$client = static::createClient();
$client->followRedirects();
$crawler = $client->request('GET', '/location/new');

$this->assertResponseIsSuccessful();
$this->assertSelectorTextContains('h1', 'Create new Location');
}

public function testShouldAddNewLocation()
{
$client = static::createClient();
$client->followRedirects();
$crawler = $client->request('GET','/location/new');

$buttonCrawlerNode = $crawler->selectButton('Save');

$form = $buttonCrawlerNode->form();

$uuid = uniqid();

$form = $buttonCrawlerNode->form([
     'location[Location]' => 'Add Location For Test . $uuid',
]);

$client->submit($form);
$this->assertResponseIsSuccessful();
$this->assertSelectorTextContains('body', 'Add Location For Test . $uuid');
}
}

