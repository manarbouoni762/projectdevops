<?php

namespace App\Tests;

use App\Entity\Voiture;

use PHPUnit\Framework\TestCase;

class VoitureTest extends TestCase
{
  public function testVoiture()
  {
    $voiture = new Voiture();
    $voiture->setSerie('serie');
    $this->assertTrue($voiture->getSerie() === 'serie');
    }
}
